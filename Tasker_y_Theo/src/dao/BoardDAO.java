package dao;

import java.sql.Connection;
import java.util.List;

import model.Board;

public interface BoardDAO {
	
	public List<Board> getAll();
	public List<Board>  getAllByUser(long userid);
	public Board get(long id);	
	public void add(Board Board);
	public void save(Board Board);
	public void delete(long id);
	public boolean find(String name, long userID);

	
	public void setConnection(Connection conn);
}
