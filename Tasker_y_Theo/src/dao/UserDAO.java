package dao;

import java.sql.Connection;

import model.User;

public interface UserDAO {
	
	public  User get(long id);
	public  User get(String name);
	public void add(User User);
	public void save(User User);
	public void delete(long id);
	public boolean find(String username);
	
	public void setConnection(Connection conn);
}
