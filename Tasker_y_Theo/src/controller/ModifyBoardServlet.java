package controller;

import java.io.IOException;
import java.sql.Connection;
import java.util.List;
import java.util.ListIterator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Board;
import model.User;
import dao.BoardDAO;
import dao.JDBCBoardDAOImpl;

/**
 * Servlet implementation class ModifyBoardServlet
 */
@WebServlet("/ModifyBoardServlet")
public class ModifyBoardServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	long idBoard;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModifyBoardServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession sesion = request.getSession();
		User user = (User)sesion.getAttribute("user");
		
		Connection conn = (Connection) getServletContext().getAttribute("dbConn");
		BoardDAO boardDB = new JDBCBoardDAOImpl();
		boardDB.setConnection(conn);
		
		
		boolean correct = false;
		List<Board> boards = boardDB.getAllByUser(user.getId());
		ListIterator<Board> iterator = boards.listIterator();
		
		while(iterator.hasNext() && !correct){
			if(iterator.next().getId() == Long.parseLong(request.getParameter("boardID")))
				correct = true;
		}
		
		if(user != null && correct){
			idBoard = Long.parseLong(request.getParameter("boardID"));
			RequestDispatcher view = request.getRequestDispatcher("WEB-INF/modifyboard.jsp");
			view.forward(request,response);
		}
		else {
			sesion.removeAttribute("user");
			sesion.invalidate();
			response.sendRedirect("Login");
		}
		

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Conexion a la base de datos
				Connection conn = (Connection) getServletContext().getAttribute("dbConn");
				BoardDAO boardDB = new JDBCBoardDAOImpl();
				boardDB.setConnection(conn);
				
				//Creamos una nueva board para modificar en la base de datos
				Board board = new Board();
				//Cogemos la sesion para pilla ID de usuario
				HttpSession sesion = request.getSession();
				User user = (User)sesion.getAttribute("user");
				//Metemos en la nueva board los parametros.
				board.setName(request.getParameter("name"));
				board.setOwner(user.getId());
				board.setId(idBoard);
				//Hacemos el update en la base de datos
				
				if(!(boardDB.find(board.getName(), user.getId()))){
					boardDB.save(board);
					response.sendRedirect("UserPage");
				}
				
				else{
					request.setAttribute("messages", "The specified name already exist");
					RequestDispatcher view = request.getRequestDispatcher("WEB-INF/modifyboard.jsp");
					view.forward(request, response);
				}
				
				
	}

}
