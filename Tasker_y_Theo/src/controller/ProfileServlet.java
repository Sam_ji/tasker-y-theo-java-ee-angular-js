package controller;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.JDBCUserDAOImpl;
import dao.UserDAO;

import model.User;

/**
 * Servlet implementation class ProfileServlet
 */
@WebServlet("/ProfileServlet")
public class ProfileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ProfileServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession sesion = request.getSession();
		User user = (User) sesion.getAttribute("user");
		if (user != null) {
			request.setAttribute("user", user);
			RequestDispatcher view = request
					.getRequestDispatcher("WEB-INF/profile.jsp");
			view.forward(request, response);
		} else {
			response.sendRedirect("Login");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		Connection conn = (Connection) getServletContext().getAttribute(
				"dbConn");
		UserDAO userDB = new JDBCUserDAOImpl();
		userDB.setConnection(conn);


		HttpSession sesion = request.getSession();
		List<String> messages = new ArrayList<String>();
		
		User user = (User) sesion.getAttribute("user");

		if (!(request.getParameter("Npassword").equals(""))) {
			if (request.getParameter("password").equals(user.getPassword())) {
				if (request.getParameter("Npassword").equals(request.getParameter("RNpassword"))) {
					user.setPassword(request.getParameter("Npassword")
							);
				} else {
					
					messages.add("Las contraseņas nuevas no coinciden, contraseņa no cambiada");
				}

			} else {
				messages.add("La contraseņa indicada no es la misma que la antigua, contraseņa no cambiada");
			}

		}
		
		if(!(request.getParameter("name").equals("")))
			user.setName(request.getParameter("name").toString());
		
		if(!(request.getParameter("email").equals("")))
			user.setEmail(request.getParameter("email").toString());
		
		userDB.save(user);
		request.setAttribute("messages",messages);
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/profile.jsp");
		view.forward(request, response);
	}

}
