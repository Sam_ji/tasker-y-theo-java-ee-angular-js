package controller;

import java.io.IOException;
import java.sql.Connection;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.User;
import dao.JDBCUserDAOImpl;
import dao.UserDAO;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(HttpServlet.class.getName());
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    

    
    public boolean PasswordMatch(User user, HttpServletRequest request){
    	boolean match = false;
    	if(user.getPassword().equals(request.getParameter("Rpassword")))
    		match = true;
    	return match;
    }
    
    

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/register.jsp");
		view.forward(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Connection conn = (Connection) getServletContext().getAttribute("dbConn");
		UserDAO userDao = new JDBCUserDAOImpl();
		userDao.setConnection(conn);
		
		User nuser = new User();
		nuser.setName(request.getParameter("name"));
		nuser.setUsername(request.getParameter("username"));
		nuser.setEmail(request.getParameter("email"));
		nuser.setPassword(request.getParameter("password"));
		

		if(!(PasswordMatch(nuser, request))){
			request.setAttribute("messages","Las contrase�as no coiciden");
			RequestDispatcher view = request.getRequestDispatcher("WEB-INF/register.jsp");
			view.forward(request,response);
		}
		
		if(userDao.find(request.getParameter("username"))){
			request.setAttribute("messages","El usuario elegido ya existe. Atr�s llama de �dun");
			RequestDispatcher view = request.getRequestDispatcher("WEB-INF/register.jsp");
			view.forward(request,response);
		}
		
		if(!(userDao.find(request.getParameter("username"))) && PasswordMatch(nuser, request)){
			userDao.add(nuser);
			request.setAttribute("messages","Su cuenta ha sido creada. Bienvenido a Tasker & Theo");
			RequestDispatcher view = request.getRequestDispatcher("WEB-INF/login.jsp");
			view.forward(request,response);
		}
		
		
		
		}	
	}


