package controller;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.smartcardio.Card;

import dao.BoardDAO;
import dao.CardDAO;
import dao.JDBCBoardDAOImpl;
import dao.JDBCCardDAOImpl;
import dao.JDBCListDAOImpl;
import dao.ListDAO;

import model.Board;
import model.User;

/**
 * Servlet implementation class BoardPageServlet
 */
@WebServlet("/BoardPageServlet")
public class BoardPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	long idBoard;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BoardPageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession sesion = request.getSession();
		User user = (User)sesion.getAttribute("user");
		Connection conn = (Connection) getServletContext().getAttribute("dbConn");
		ListDAO listDB = new JDBCListDAOImpl();
		BoardDAO boardBD = new JDBCBoardDAOImpl();
		CardDAO cardDB = new JDBCCardDAOImpl();
		boardBD.setConnection(conn);
		listDB.setConnection(conn);
		cardDB.setConnection(conn);
		boolean correct = false;
		
		
		List<Board> boards = boardBD.getAllByUser(user.getId());
		List<model.Card> cards = cardDB.getAll();
		ListIterator<Board> iterator = boards.listIterator();
		try{
		idBoard = Long.parseLong(request.getParameter("boardID"));
		}
		catch(Exception e){}
		
		while(iterator.hasNext() && !correct){
			if(iterator.next().getId() == idBoard)
				correct = true;
		}
		
		if(user != null && correct){

			List<model.List> lists = listDB.getAllByBoard(idBoard);
			request.setAttribute("boardID", idBoard);
			request.setAttribute("cards", cards);
			request.setAttribute("lists", lists);
			RequestDispatcher view = request.getRequestDispatcher("WEB-INF/boardpage.jsp");
			view.forward(request,response);
		}
		else {
			sesion.removeAttribute("user");
			sesion.invalidate();
			response.sendRedirect("Login");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			Connection conn = (Connection) getServletContext().getAttribute("dbConn");
			CardDAO cardDB = new JDBCCardDAOImpl();
			cardDB.setConnection(conn);
			
			model.Card card = new model.Card();
			
			card.setName(request.getParameter("cardName"));
			card.setList(Long.parseLong(request.getParameter("option")));
			card.setId(Long.parseLong(request.getParameter("cardID")));
			
			cardDB.save(card);
			
			response.sendRedirect("BoardPage?boardID="+idBoard);
	}

}
