package controller;

import java.io.IOException;
import java.sql.Connection;
import java.util.List;
import java.util.ListIterator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BoardDAO;
import dao.JDBCBoardDAOImpl;
import dao.JDBCListDAOImpl;
import dao.ListDAO;

import model.Board;
import model.User;

/**
 * Servlet implementation class NewListServlet
 */
@WebServlet("/NewListServlet")
public class NewListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	long boardID;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession sesion = request.getSession();
		User user = (User)sesion.getAttribute("user");
		

		Connection conn = (Connection) getServletContext().getAttribute("dbConn");
		BoardDAO boardDB = new JDBCBoardDAOImpl();
		boardDB.setConnection(conn);
		
		
		boolean correct = false;
		List<Board> boards = boardDB.getAllByUser(user.getId());
		ListIterator<Board> iterator = boards.listIterator();
		
		while(iterator.hasNext() && !correct){
			if(iterator.next().getId() == Long.parseLong(request.getParameter("boardID")))
				correct = true;
		}
		
		
		if(user != null && correct){
		boardID = Long.parseLong(request.getParameter("boardID"));
		request.setAttribute("boardID", boardID);
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/newlist.jsp");
		view.forward(request, response);
		}
		
		else{
			response.sendRedirect("Login");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Connection conn = (Connection) getServletContext().getAttribute("dbConn");
		ListDAO listDB = new JDBCListDAOImpl();
		listDB.setConnection(conn);
		
		model.List list = new model.List();
		
		list.setBoard(boardID);
		list.setName(request.getParameter("name"));
		
		if(!(listDB.find(list.getName(), list.getBoard()))){
			response.sendRedirect("BoardPage?boardID="+boardID);
			listDB.add(list);
		}
		else{
			request.setAttribute("messages", "La lista ya existe");
			request.setAttribute("boardID", boardID);
			RequestDispatcher view = request.getRequestDispatcher("WEB-INF/newlist.jsp");
			view.forward(request, response);
		}
		
		
		
		
		
	}

}
