package controller;

import java.io.IOException;
import java.sql.Connection;
import java.util.List;
import java.util.ListIterator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.smartcardio.Card;

import model.Board;
import model.User;
import dao.BoardDAO;
import dao.CardDAO;
import dao.JDBCBoardDAOImpl;
import dao.JDBCCardDAOImpl;

/**
 * Servlet implementation class NewCardServlet
 */
@WebServlet("/NewCardServlet")
public class NewCardServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	long boardID;
	long listID;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewCardServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession sesion = request.getSession();
		User user = (User)sesion.getAttribute("user");
		

		Connection conn = (Connection) getServletContext().getAttribute("dbConn");
		BoardDAO boardDB = new JDBCBoardDAOImpl();
		boardDB.setConnection(conn);
		
		
		boolean correct = false;
		List<Board> boards = boardDB.getAllByUser(user.getId());
		ListIterator<Board> iterator = boards.listIterator();
		
		while(iterator.hasNext() && !correct){
			if(iterator.next().getId() == Long.parseLong(request.getParameter("boardID")))
				correct = true;
		}
		
		
		if(user != null && correct){
		boardID = Long.parseLong(request.getParameter("boardID"));
		listID = Long.parseLong(request.getParameter("listID"));
		request.setAttribute("boardID", boardID);
		request.setAttribute("listID", listID);
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/newcard.jsp?");
		view.forward(request, response);
		}
		
		else{
			sesion.removeAttribute("user");
			sesion.invalidate();
			response.sendRedirect("Login");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Connection conn = (Connection) getServletContext().getAttribute("dbConn");
		CardDAO cardDB = new JDBCCardDAOImpl();
		cardDB.setConnection(conn);
		
		model.Card card = new model.Card();
		
		card.setList(listID);
		card.setName(request.getParameter("name"));
		
		cardDB.add(card);
		
		response.sendRedirect("BoardPage?boardID="+boardID);
		
	}

}
