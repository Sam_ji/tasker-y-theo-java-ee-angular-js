package controller;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BoardDAO;
import dao.JDBCBoardDAOImpl;
import java.util.List;
import model.Board;
import model.User;

/**
 * Servlet implementation class UserPageSerlvlet
 */
@WebServlet("/UserPageSerlvlet")
public class UserPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserPageServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession sesion = request.getSession();
		User user = (User) sesion.getAttribute("user");
		if (user != null) {
			// Hacemos la conexion a la base de datos de las boards
			Connection conn = (Connection) getServletContext().getAttribute(
					"dbConn");
			BoardDAO boardDB = new JDBCBoardDAOImpl();
			boardDB.setConnection(conn);

			// Nos creamos una lista de boards

			List<Board> boards = boardDB.getAllByUser(user.getId());
			//Se lo metemos en atributos
			request.setAttribute("boards", boards);
			RequestDispatcher view = request
					.getRequestDispatcher("WEB-INF/userpage.jsp");
			view.forward(request, response);

		} else {
			response.sendRedirect("Login");
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher view = request
				.getRequestDispatcher("WEB-INF/userpage.jsp");
		view.forward(request, response);
	}

}
