package controller;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Board;
import model.User;

import dao.BoardDAO;
import dao.JDBCBoardDAOImpl;

/**
 * Servlet implementation class NewBoardServlet
 */
@WebServlet("/NewBoardServlet")
public class NewBoardServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewBoardServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession sesion = request.getSession();
		User user = (User)sesion.getAttribute("user");
		if(user != null){
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/newboard.jsp");
		view.forward(request,response);
		}
		else {
			response.sendRedirect("Login");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Conexion a la base de datos
		Connection conn = (Connection) getServletContext().getAttribute("dbConn");
		BoardDAO boardDB = new JDBCBoardDAOImpl();
		boardDB.setConnection(conn);
		
		//Creamos una nueva board para insertarla en la base de datos
		Board board = new Board();
		//Cogemos la sesion para pilla ID de usuario
		HttpSession sesion = request.getSession();
		User user = (User)sesion.getAttribute("user");
		//Metemos en la nueva board los parametros.
		board.setName(request.getParameter("name"));
		board.setOwner(user.getId());
		
		if(!(boardDB.find(request.getParameter("name"), user.getId()))){
			boardDB.add(board);
			response.sendRedirect("UserPage");
		}
		else{
			request.setAttribute("messages", "El nombre de la tabla especificado ya existe");
			RequestDispatcher view = request.getRequestDispatcher("WEB-INF/newboard.jsp");
			view.forward(request,response);
		}
		
	}

}
