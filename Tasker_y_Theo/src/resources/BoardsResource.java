package resources;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import dao.JDBCBoardDAOImpl;
import dao.BoardDAO;
import model.Board;
import model.User;
import resources.exceptions.CustomBadRequestException;
import resources.exceptions.CustomNotFoundException;

@Path("/boards")
public class BoardsResource {

	@Context
	ServletContext sc;
	@Context
	UriInfo uriInfo;
	@Context
	HttpServletRequest request;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Board> getBoardsJSON() {
		Connection conn = (Connection) sc.getAttribute("dbConn");
		BoardDAO BoardDao = new JDBCBoardDAOImpl();
		BoardDao.setConnection(conn);
		long user = Long.parseLong(request.getSession().getAttribute("user").toString());

		List<Board> Boards = BoardDao.getAllByUser(user);
		return Boards;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String newBoard(@QueryParam("name") String name) {
		String correct = "{\"ok\":\"false\"}";
		Connection conn = (Connection) sc.getAttribute("dbConn");
		BoardDAO BoardDao = new JDBCBoardDAOImpl();
		BoardDao.setConnection(conn);
		long user = Long.parseLong(request.getSession().getAttribute("user").toString());

		if (!BoardDao.find(name, user)) {
			Board board = new Board();
			board.setName(name);
			board.setOwner(user);
			BoardDao.add(board);
			correct = "{\"ok\":\"true\"}";
		}
		return correct;

	}

	@DELETE
	@Path("/{boardid:[0-9]+}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deleteBoard(@PathParam("boardid") long boardid) {
		Connection conn = (Connection) sc.getAttribute("dbConn");
		BoardDAO BoardDao = new JDBCBoardDAOImpl();
		BoardDao.setConnection(conn);
		Board b = BoardDao.get(boardid);
		long user = Long.parseLong(request.getSession().getAttribute("user").toString());
		// Comprobaci�n de que quien lo est� borrando es el usuario

		System.out.println("Borrando");
		if (b.getOwner() == user) {
			BoardDao.delete(boardid);
		} else {
			request.getSession().invalidate();
		}

		Response respuesta = Response
				.created(
						uriInfo.getAbsolutePathBuilder()
								.path(Long.toString(boardid)).build())
				.contentLocation(
						uriInfo.getAbsolutePathBuilder()
								.path(Long.toString(boardid)).build()).build();

		return respuesta;
	}

	@PUT
	@Path("/{boardid:[0-9]+}")
	@Consumes(MediaType.APPLICATION_JSON)
	public String modifyTablon(@PathParam("boardid") long boardid,
			@QueryParam("name") String name) {
		String correct = "{\"ok\":\"false\"}";
		Connection conn = (Connection) sc.getAttribute("dbConn");
		BoardDAO BoardDao = new JDBCBoardDAOImpl();
		BoardDao.setConnection(conn);
		Board b = BoardDao.get(boardid);
		b.setName(name);
		long user = Long.parseLong(request.getSession().getAttribute("user").toString());
		System.out.println("Trying to update board " + boardid + "by user " + user );
		if (!BoardDao.find(name, user) && b.getOwner() == user) {
			BoardDao.save(b);
			correct = "{\"ok\":\"true\"}";
		}
		return correct;
	}
}
