package resources;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.naming.java.javaURLContextFactory;

import dao.JDBCCardDAOImpl;
import dao.CardDAO;
import model.User;
import model.Card;
import resources.exceptions.CustomBadRequestException;
import resources.exceptions.CustomNotFoundException;

@Path("/cards")
public class CardsResource {

	@Context
	ServletContext sc;
	@Context
	UriInfo uriInfo;
	@Context
	HttpServletRequest request;

	@GET
	@Path("/{listid: [0-9]+}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<model.Card> getCardsJSON(@PathParam("listid") long listid) {
		Connection conn = (Connection) sc.getAttribute("dbConn");
		CardDAO CardDao = new JDBCCardDAOImpl();
		CardDao.setConnection(conn);

		List<model.Card> cards = CardDao.getAllByList(listid);
		System.out.println("Obteniendo cards y destruyendo la navidad" + cards);
		return cards;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public void createCard(@QueryParam("listid") long listid,
			@QueryParam("name") String name) {
		Connection conn = (Connection) sc.getAttribute("dbConn");
		CardDAO CardDao = new JDBCCardDAOImpl();
		CardDao.setConnection(conn);

		Card card = new Card();
		card.setName(name);
		card.setList(listid);
		CardDao.add(card);
	}

	@DELETE
	@Path("/{cardid: [0-9]+}")
	@Consumes(MediaType.APPLICATION_JSON)
	public void deleteCard(@PathParam("cardid") long cardid) {
		Connection conn = (Connection) sc.getAttribute("dbConn");
		CardDAO CardDao = new JDBCCardDAOImpl();
		CardDao.setConnection(conn);

		CardDao.delete(cardid);
	}

	@PUT
	@Path("/{cardid: [0-9]+}")
	@Consumes(MediaType.APPLICATION_JSON)
	public void deleteCard(@PathParam("cardid") long cardid,
			@QueryParam("name") String name, @QueryParam("newlist") long newlist) {
		Connection conn = (Connection) sc.getAttribute("dbConn");
		CardDAO CardDao = new JDBCCardDAOImpl();
		CardDao.setConnection(conn);

		
		Card card = CardDao.get(cardid);
		if (name!= null) {
			card.setName(name);
		}
		if(newlist != 0){
			card.setList(newlist);
		}
		CardDao.save(card);

	}

}
