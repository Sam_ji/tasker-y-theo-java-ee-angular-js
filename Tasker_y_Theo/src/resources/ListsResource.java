package resources;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.naming.java.javaURLContextFactory;

import dao.BoardDAO;
import dao.JDBCBoardDAOImpl;
import dao.JDBCListDAOImpl;
import dao.ListDAO;
import model.Board;
import model.User;
import resources.exceptions.CustomBadRequestException;
import resources.exceptions.CustomNotFoundException;

@Path("/lists")
public class ListsResource {

	@Context
	ServletContext sc;
	@Context
	UriInfo uriInfo;
	@Context
	HttpServletRequest request;

	@GET
	@Path("/{boardid: [0-9]+}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<model.List> getListsJSON(@PathParam("boardid") long boardid) {
		Boolean correct = false;
		Connection conn = (Connection) sc.getAttribute("dbConn");
		ListDAO ListDao = new JDBCListDAOImpl();
		BoardDAO BoardDao = new JDBCBoardDAOImpl();
		BoardDao.setConnection(conn);
		ListDao.setConnection(conn);
		List<model.List> lists = new ArrayList<model.List>();
		Long user = Long.parseLong(request.getSession().getAttribute("user")
				.toString());
		List<Board> boards = BoardDao.getAllByUser(user);
		ListIterator<Board> iterator = boards.listIterator();
		// Aqui se controla que est� accediendo a una de sus boards
		while (iterator.hasNext() && !correct) {
			if (iterator.next().getId() == boardid)
				correct = true;
		}
		if (correct)
			lists = ListDao.getAllByBoard(boardid);

		System.out.println("Pillando lists..." + lists);
		return lists;
	}

	@POST
	@Path("/{boardid: [0-9]+}")
	@Produces(MediaType.APPLICATION_JSON)
	public String newList(@PathParam("boardid") long boardid,
			@QueryParam("name") String name) {
		String correct = "{\"ok\":\"false\"}";
		Connection conn = (Connection) sc.getAttribute("dbConn");
		ListDAO ListDao = new JDBCListDAOImpl();
		ListDao.setConnection(conn);
		long user = Long.parseLong(request.getSession().getAttribute("user")
				.toString());

		if (!ListDao.find(name, user)) {
			model.List list = new model.List();
			list.setName(name);
			list.setBoard(boardid);
			ListDao.add(list);
			correct = "{\"ok\":\"true\"}";
			System.out.println("List created by " + "user " + user);
		}
		return correct;

	}

	@DELETE
	@Path("/{listid: [0-9]+}")
	@Consumes(MediaType.APPLICATION_JSON)
	public void deleteList(@QueryParam("listid") long listid, @QueryParam("boardid") long boardid) {
		Connection conn = (Connection) sc.getAttribute("dbConn");
		ListDAO ListDao = new JDBCListDAOImpl();
		ListDao.setConnection(conn);
		ListDao.delete(listid);
	}
	
	@PUT
	@Path("/{listid:[0-9]+}")
	@Consumes(MediaType.APPLICATION_JSON)
	public void modifyTablon(@PathParam("listid") long listid,
			@QueryParam("name") String name, @QueryParam("boardid") long boardid ) {
		Connection conn = (Connection) sc.getAttribute("dbConn");
		ListDAO ListDao = new JDBCListDAOImpl();
		ListDao.setConnection(conn);
		model.List list = ListDao.get(listid);
		list.setBoard(boardid);
		System.out.println(list.getBoard());
		System.out.println(list.getId());
		list.setName(name);
		ListDao.save(list);

	}
}