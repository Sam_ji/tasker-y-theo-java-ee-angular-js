package resources;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import dao.JDBCUserDAOImpl;
import dao.UserDAO;
import model.User;
import resources.exceptions.CustomBadRequestException;
import resources.exceptions.CustomNotFoundException;

@Path("/Users")
public class UsersResource {

	  @Context
	  ServletContext sc;
	  @Context
	  UriInfo uriInfo;
	  @Context
	  HttpServletRequest request; //Contexto para guardar la sesion. Hay que activarlo en todos los sitios de la API 
  
	  @GET
	 @Produces(MediaType.APPLICATION_JSON)
	  public String authUsersJSON(@QueryParam("username") String name, @QueryParam("password")String password) {
		String correct = "{\"ok\":\"false\"}"; //Hay que montar todo este pitote de las comillacas para que pille la cadena js
		Connection conn = (Connection) sc.getAttribute("dbConn");
		UserDAO UserDao = new JDBCUserDAOImpl();
		UserDao.setConnection(conn);			
		
		User user = UserDao.get(name);
		
		if(user!=null && user.getPassword().equals(password)){
			correct = "{\"ok\":\"true\"}"; //el ok es para que accedamos en el controller a la cadena bien formada en el campo "ok" de los datos
			request.getSession().setAttribute("user", user.getId()); //guardamos en sesion
			
		}
		
		return correct;
	  }
	  
	  @GET
	  @Path("/{Userid: [0-9]+}")	  
	  @Produces(MediaType.APPLICATION_JSON)
	  public User getUserJSON(@PathParam("Userid") long Userid) {
		  
		Connection conn = (Connection) sc.getAttribute("dbConn");
		UserDAO UserDao = new JDBCUserDAOImpl();
		UserDao.setConnection(conn);
		
		User User = UserDao.get(Userid);
		if (User == null) {
		    throw new CustomNotFoundException("User, " + Userid + ", is not found");
		  }
		
	    return User; 
	  }
	  
	
} 
