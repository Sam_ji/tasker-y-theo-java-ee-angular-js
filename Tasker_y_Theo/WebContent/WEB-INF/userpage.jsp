<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="css/styles.css"/>
	<title>Tasker & Theo</title>
</head>
<body>

	<div id="header">
		<div> 
			<h1><a href="index.html">Tasker & Theo</a></h1> <!-- Esto a la izquierda del todo -->
			<div class="region">
				<a href="Logout">LogOut</a>
				
				<a href="UserPage">User Page</a> 
				<a href="NewBoard">New Board</a>
				<a href="Profile">Profile</a>
			</div>
		</div>
	</div>
	<div class="main">
		<ul class="dat_ul">
			<c:forEach var="board" items="${boards}">
				<li>
					<p><a class="board_name" href="BoardPage?boardID=${board.id}">${board.name}</a></p>
					<a href="ModifyBoard?boardID=${board.id}">Modify Board</a>
					<a href="DeleteBoard?boardID=${board.id}">Delete board</a>
	
				</li>
			</c:forEach>
		</ul>
	</div>
	<div id="footer"> 
		<div>
			&#169 by Sancho Muñoz Jimenez	
		</div>
	</div>
</form>
</body>
</html>
