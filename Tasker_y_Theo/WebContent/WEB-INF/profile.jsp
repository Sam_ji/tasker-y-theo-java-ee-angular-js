<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="css/styles.css"/>
	<title>Tasker & Theo</title>
</head>
<body>
	
	<div id="header">
		<div> 
			<h1>
				<a href="UserPage">Tasker & Theo</a>
			</h1> 
			<div class="region">
						
				<a href="Logout">LogOut</a>
				<!--Botones-->
				<a href="UserPage">User Page</a> 
				<a href="NewBoard">New Board</a> 
			</div>
		</div>
	</div>
	<div class="main">
		<div class="dalogin">
			<div>
				<h1>This is your actual profile</h1>
				<p>Username: ${user.username}</p>
				<p>Name: ${user.name}</p>
				<p>Email: ${user.email}</p>
			</div>
			<div>Insert the new data of yours. 
				<p>Leave empty the fields that you don't want to modify</p>
				<form method="POST" action="Profile">
						<div>
							<label>Old password:</label>
							<input type="password" name="password" />
						</div>
						<div>
							<label>New password:</label>
							<input type="password" name="Npassword" />
						</div>
						<div>
							<label>Repeat new password</label>
							<input type="password" name="RNpassword" />
						</div>
						<div>
							<label>Name:</label>
							<input type="text" name="name" />
						</div>
						<div>
							<label>email:</label>
							<input type="email" name="email" />
						</div>

						<input type="submit" value="Enter">
					</form>
					</div>
					
					<c:forEach var="message" items="${messages}">
								<p>${message}</p>
					 </c:forEach>

		</div>
	</div>
	<div id="footer"> 
		<div>
			&#169 by Sancho Muñoz Jimenez	
		</div>
	</div>
</form>
</body>
</html>