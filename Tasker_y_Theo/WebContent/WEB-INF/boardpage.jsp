<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="css/styles.css" />
<title>Tasker & Theo</title>
</head>
<body>


	<div id="header">
		<div>
			
			<h1>
				<a href="UserPage">Tasker & Theo</a>
			</h1>
			<div class="region">
				<a href="Logout">LogOut</a> <a href="Profile">Profile</a> 
				<a href="UserPage">User page</a> 
				<a href="BoardPage?boardID=${boardID}">Board page</a> 
				<a href="NewBoard">New Board</a> <a href="NewList?boardID=${boardID}">New List</a>
			</div>
		</div>
	</div>
	<div class="main">
		<div class="dalogin">
			<c:forEach var="list" items="${lists}">
				<div>
					<p class="list-name">${list.name}</p>
					<a href="NewCard?boardID=${boardID}&listID=${list.id}">Create new card for this list</a> 
					<a href="ModifyList?boardID=${boardID}&listID=${list.id}">Rename list</a> 
					<a href="DeleteList?boardID=${boardID}&listID=${list.id}">Delete list</a>
					<ul class="da_card">
						<c:forEach var="card" items="${cards}">
							<li>
								<c:if test="${list.id == card.list}">
									<p class="card-name">${card.name}</p>
									<a href="ModifyCard?boardID=${boardID}&cardID=${card.id}&listID=${list.id}">Rename card</a>
									<a href="DeleteCard?boardID=${boardID}&cardID=${card.id}&listID=${list.id}">Delete card</a>
									<form action="BoardPage?cardID=${card.id}&cardName=${card.name}" method="POST">
										<label for="option">Move card to</label> <select name="option">
											<c:forEach var="lista" items="${lists}">
												<option value="${lista.id}">${lista.name}</option>
											</c:forEach>
										</select> 
										<input type="submit" name="OK" value="OK">
									</form>
								</c:if>
							</li>
						</c:forEach>
					</ul>
				</div>
			</c:forEach>
		</div>
	</div>
	<div id="footer">
		<div>&#169 by Sancho Muñoz Jimenez</div>
	</div>
</body>
</body>
</html>