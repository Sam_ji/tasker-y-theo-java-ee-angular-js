<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="css/styles.css"/>
	<title>Tasker & Theo</title>
</head>
<body>
	
	
	<div id="header">
		<div> <!-- Navbar -->
			<h1><a href="index.html">Tasker & Theo</a></h1> 
			<div class="region">
				<a href="Login">Login</a> <!--Botones-->
				<a href="Register">Register</a>
			</div>
		</div>
	</div>
	<div class="main">

		<div class="dalogin"> 
			<h1>JOIN US JOIN US</h1>
			<form method="POST" action="Register">
				<div>
					<label>User:</label>
					<input type="text" name="username" required="required" />
				</div>
				<div>
					<label>Name:</label>
					<input type="text" name="name" required="required" />
				</div>
				<div>
					<label>Password:</label>
					<input type="password" name="password" required="required" />
				</div>
				<div>
					<label>Repeat password</label>
					<input type="password" name="Rpassword" required="required" />
				</div>
				<div>
					<label>email:</label>
					<input type="mail" name="email" required="required">
				</div>

				<input type="submit" value="Enter">
			</form>
			<p>${messages}</p>
		</div>
	</div>
	<div id="footer"> 
		<div>
			&#169 by Sancho Muñoz Jimenez	
		</div>
	</div>
</form>
</body>
</html>
