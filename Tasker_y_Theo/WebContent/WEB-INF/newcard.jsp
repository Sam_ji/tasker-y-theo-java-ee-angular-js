<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="css/styles.css"/>
	<title>Tasker & Theo</title>
</head>
<body>
	
	
	<div id="header">
		<div> <!-- Navbar -->
			<h1>
				<a href="UserPage">Tasker & Theo</a>
			</h1> 
			<div class="region">
				<a href="Logout">LogOut</a>
				<!--Botones-->
				<a href="Profile">Profile</a>
				<a href="UserPage">User page</a>
				<a href="BoardPage?boardID=${boardID}">Board page</a>
			</div>
		</div>
	</div>
	<div class="main">
		<div class="dalogin"> 
			<form method="POST" action="NewCard">
				<div>
					<label>Insert the name of the card</label>
					<input type="text" name="name" required="required" />
				</div>
				<input type="submit" value="Enter">
			</form>
			<p>${messages}</p>
		</div>
	</div>
	<div id="footer"> 
		<div>
			&#169 by Sancho Muñoz Jimenez	
		</div>
	</div>
</form>
</body>
</html>