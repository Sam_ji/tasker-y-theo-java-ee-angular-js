<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="css/styles.css"/>
	<title>Tasker & Theo</title>
</head>
<body>
	
	
	<div id="header">
		<div> 
			<h1><a href="index.html">Tasker & Theo</a></h1> 
			<div class="region">
				<a href="Login">Login</a> 
				<a href="Register">Register</a>
			</div>
		</div>
	</div>
	<div class="main">
		<div class="dalogin">
			<h1>Enter your credentials</h1> 
				<form method="POST" action="Login">
					<div>
				  		<label>User:</label>
				  		<input type="text" name="username" />
				  	</div>
				  	<div>
				  		<label>Password:</label>
				  		<input type="password" name="password" />
				  	</div>
				  <input type="submit" value="Enter"> 
				</form>
				<p>${messages}</p> 
		</div>
	</div>
	<div id="footer"> 
		<div>
			&#169 by Sancho Muñoz Jimenez	
		</div>
	</div>
</form>
</body>
</html>