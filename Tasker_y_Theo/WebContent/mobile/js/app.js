angular.module('myApp', [
	'ngRoute',
	'Filters',
	'MyServices',
	'Controllers'
]).
config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/login', {templateUrl: 'partials/login.html', controller: 'Login'});
	$routeProvider.when('/boards', {templateUrl: 'partials/boards.html', controller: 'Boards'});
	$routeProvider.when('/create_board', {templateUrl: 'partials/create_board.html', controller: 'AddBoard'});
	$routeProvider.when('/delete_board/:boardid', {templateUrl: 'partials/delete_board.html', controller: 'DeleteBoard'});
	$routeProvider.when('/update_board/:boardid', {templateUrl: 'partials/update_board.html', controller: 'UpdateBoard'});
	$routeProvider.when('/lists/:boardid', {templateUrl: 'partials/lists.html', controller: 'Lists'});
	$routeProvider.when('/create_list', {templateUrl: 'partials/create_list.html', controller: 'AddList'});
	$routeProvider.when('/delete_list/:listid', {templateUrl: 'partials/delete_list.html', controller: 'DeleteList'});
	$routeProvider.when('/update_list/:listid', {templateUrl: 'partials/update_list.html', controller: 'UpdateList'});
	$routeProvider.when('/cards/:listid', {templateUrl: 'partials/cards.html', controller: 'Cards'});
	$routeProvider.when('/create_card', {templateUrl: 'partials/create_card.html', controller: 'AddCard'});
	$routeProvider.when('/delete_card/:cardid', {templateUrl: 'partials/delete_card.html', controller: 'DeleteCard'});
	$routeProvider.when('/update_card/:cardid', {templateUrl: 'partials/update_card.html', controller: 'UpdateCard'});
	$routeProvider.otherwise({redirectTo: '/login'});
}]);
