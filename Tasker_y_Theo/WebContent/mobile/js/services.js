var MyServices = angular.module('MyServices', ['ngResource']);
MyServices.factory('Board', ['$resource',
	function($resource){
		return $resource('/Tasker_y_Theo/rest/boards/:boardid', {}, {
			All: {method:'GET', params:{boardId:''}},
			Add: {method:'POST', params:{name:'@name'}},
			Del: {method:'DELETE', params:{boardid:'@boardid'}},
			Upd: {method:'PUT', params:{boardid:'@boardid', name:'@name'}}
	});
}]);

MyServices.factory('User', ['$resource',
                         	function($resource){
                         		return $resource('/Tasker_y_Theo/rest/Users', {}, {});
                         }]);
MyServices.factory('List', ['$resource',
                         	function($resource){
                         		return $resource('/Tasker_y_Theo/rest/lists/:boardid', {}, {
                         			All: {method:'GET', params:{boardid:'@boardid'}},
                         			Add: {method:'POST', params:{boardid:'@boardid', name:'@name'}},
                         			Del: {method:'DELETE', params:{listid:'@listid', boardid:'@boardid'}}
                         			
                         		});
                         }]);
MyServices.factory('List2', ['$resource',
                         	function($resource){
                         		return $resource('/Tasker_y_Theo/rest/lists/:listid', {}, {
                         			Upd: {method:'PUT', params:{listid:'@listid', name:'@name', boardid:'@boardid'}}
                         		});
                         }]);

MyServices.factory('Card', ['$resource',
                         	function($resource){
                         		return $resource('/Tasker_y_Theo/rest/cards/:listid', {}, {
                         			All: {method:'GET', params:{boardid:'@listid'}}
                         		});
                         }]);
MyServices.factory('Card2', ['$resource',
                         	function($resource){
                         		return $resource('/Tasker_y_Theo/rest/cards', {}, {
                         			Add: {method:'POST', params:{listid:'@listid', name: '@name'}}
                         		});
                         }]);
MyServices.factory('Card3', ['$resource',
                          	function($resource){
                          		return $resource('/Tasker_y_Theo/rest/cards/:cardid', {}, {
                          			Del: {method:'DELETE', params:{cardid:'@cardid'}},
                          			Upd:{method: 'PUT', params:{cardid:'@cardid', name: '@name', newlist:'@newlist'}}
                          		});
                          }]);