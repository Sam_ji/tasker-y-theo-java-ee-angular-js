var board = 0;
var list = 0;
var lists = [];
angular.module('Controllers', [])
	.controller('Login', ['$scope','User',function($scope, User ) {
		
		$scope.Auth = function(){
			  User.get({ username: $scope.username, password: $scope.password })
			    .$promise.then( function( data) { //si no rula cambiar .$promise.then por .success
			       if( data.ok == "true" ) 
			           location.hash = "/boards";
			    });
			    
			};
	}])
	
	
	
		.controller('AddBoard',['$scope', 'Board', function($scope, Board){
			$scope.n="";
			$scope.Add = function(){
				Board.Add({name : $scope.name}).$promise.then(function(data){
					if(data.ok == "true")
						location.hash = "/boards";
					if(data.ok == "false")
						$scope.n = "the name already exists";
				});
			};
	}])
	
	.controller('DeleteBoard',['$scope','$routeParams', 'Board', function($scope, $routeParams ,Board){
			$scope.Del = function(){
				Board.Del({boardid : $routeParams.boardid}).$promise.then(function(data){
					location.hash = "/boards";
				});
			};
	}])
	
	.controller('UpdateBoard',['$scope','$routeParams', 'Board', function($scope, $routeParams ,Board){
		$scope.n = "";	
		$scope.Upd = function(){
				Board.Upd({boardid : $routeParams.boardid, name: $scope.name}).$promise.then(function(data){
					if(data.ok == "true"){
					location.hash = "/boards";
					}
					if(data.ok == "false"){
						$scope.n = "The name already exist. Try again";
					}
				});
			};
	}])
	
		.controller('Lists', ['$scope','$routeParams','List',function($scope,$routeParams, List ) {
			board = $routeParams.boardid;
			List.All({boardid : $routeParams.boardid}).$promise.then(function(data){
				$scope.lists = _([data.list]).flatten();
				lists = $scope.lists;
				console.log(lists);
				if(typeof($scope.lists[0])=="undefined")
					$scope.lists = [];
				
			});
		
	}])
	
	.controller('AddList',['$scope','$routeParams' ,'List', function($scope, $routeParams, List){
			$scope.n="";
			$scope.Add = function(){
				List.Add({boardid :board ,name : $scope.name}).$promise.then(function(data){
					if(data.ok == "true")
						location.hash = "/lists/" + board;
					if(data.ok == "false")
						$scope.n = "the name already exists";
				});
			};
	}])
	.controller('DeleteList',['$scope','$routeParams', 'List', function($scope, $routeParams ,List){
			$scope.b = board;
			$scope.listid = $routeParams.listid;
			$scope.Del = function(){
				List.Del({listid : $scope.listid, boardid: board}).$promise.then(function(data){
					location.hash = "/lists/" + board;
					
				});
			};
	}])
	
	
	
	.controller('UpdateList',['$scope','$routeParams', 'List2', function($scope, $routeParams ,List2){
		$scope.b = board;
		$scope.Upd = function(){
				List2.Upd({listid : $routeParams.listid , name: $scope.name, boardid: $scope.b}).$promise.then(function(data){
					location.hash = "/lists/" + board;
					
				});
			};
	}])
	
	
	.controller('Cards', ['$scope','$routeParams','Card',function($scope,$routeParams, Card ) {
			list = $routeParams.listid;
			Card.All({listid : $routeParams.listid}).$promise.then(function(data){
				$scope.cards = _([data.card]).flatten();
				if(typeof($scope.cards[0])=="undefined")
					$scope.cards = [];
			});
		
	}])
	
	.controller('AddCard',['$scope','$routeParams' ,'Card2', function($scope, $routeParams, Card2){
			$scope.list= list;
			$scope.Add = function(){
				Card2.Add({listid :$scope.list ,name : $scope.name}).$promise.then(function(data){
						location.hash = "/cards/" + list;
				});
			};
	}])
	
	.controller('DeleteCard',['$scope','$routeParams', 'Card3', function($scope, $routeParams ,Card3){
			$scope.Del = function(){
				Card3.Del({cardid: $routeParams.cardid}).$promise.then(function(data){
					location.hash = "/cards/" + list;
					
				});
			};
	}])
	
	.controller('UpdateCard',['$scope','$routeParams', 'Card3', function($scope, $routeParams ,Card3){
		$scope.listas = lists;
		
		$scope.Upd = function(){
				Card3.Upd({cardid : $routeParams.cardid , name: $scope.name, newlist:$scope.list_d }).$promise.then(function(data){
					location.hash = "/lists/" + board;
					console.log($scope.list_d);
				});
			};
	}])
	
	
	
	
	
	.controller('Boards', ['$scope', 'Board', function($scope, Board) {
		Board.All().$promise.then(function(data){
		$scope.boards = _([data.board]).flatten();
		if(typeof($scope.boards[0]) == "undefined"){
			$scope.boards = [];
		}});
		
		
		
	}]);